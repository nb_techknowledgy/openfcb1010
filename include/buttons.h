/* ****************************************************************/
/* File Name    : buttons.h                                       */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __BUTTONS_H
#define __BUTTONS_H

#define PRESS_COUNT		(20)

#define	RELEASED	(0)
#define PRESSED		(1)
#define HELD		(2)
#define	TOE_1		(11)
#define	TOE_2		(12)
#define HEEL_1		(13)
#define HEEL_2		(14)
#define UP			(15)
#define	DOWN		(16)
#define LAST_KEY	(DOWN+1)


#define CONFIRM_COUNT	(100)
#define SETUP_TIME		(100)
#define	DEBOUNCE_TIME	(10)

//	extern			volatile	uint8_t	Local_CS_PER;	// 	Last value written to keypad scan latch
extern			volatile	uint8_t	KeyColumn;
extern	__idata	volatile	uint8_t	CurrentKeyState[20];
extern	__idata	volatile	uint8_t	PreviousKeyState[20];
extern	__idata	volatile	uint8_t	KeyHeldForCount[20];
extern	__idata				uint8_t	Button[20];
extern			const		uint8_t KeyMap[];


void BUTTONS_Update();

#endif
