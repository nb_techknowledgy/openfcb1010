/* ****************************************************************/
/* File Name    : uart.h                                          */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __UART_H
#define	__UART_H

void		UART_Init();
void		UART_ISR (void) __interrupt 4;
uint16_t	UART_ReceiveLen();
uint16_t	UART_TransmitLen();
uint8_t		UART_Receive();
uint16_t	UART_StartTx();
uint8_t		UART_Transmit(uint8_t Byte);
void		UART_DumpRxBuff();

#endif
