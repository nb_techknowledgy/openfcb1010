/* ****************************************************************/
/* File Name    : pedals.c                                        */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include "common.h"
#include "adc.h"
#include "pedals.h"

		uint8_t	LeftPedal;
		uint8_t	RightPedal;
		uint8_t LastLeft;
		uint8_t	LastRight;
const	uint8_t	DeadBand = 3;

void PEDALS_GetPositions()
{
	LeftPedal = ADC_Read(0);
	RightPedal = ADC_Read(1);
}
