/* ****************************************************************/
/* File Name    : test.c                                          */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include "common.h"
#include "adc.h"
#include "buttons.h"
#include "display.h"
#include "nvram.h"
#include "pedals.h"
#include "timers.h"
#include "uart.h"
#include "utils.h"


#define PROGRAM_CHANGE(chan)	(0xC0 | (chan & 0x0F)
#define CONTROL_CHANGE(chan)	(0xB0 | (chan & 0x0F)


const char *Messages[] = {
	"\xC0\x0\xC1\x0\xC2\x0\xC3\x0\xC4\x0\xb0\x0\x0\xb1\x0\x1F",		//	0
	"\xC0\x1\xC1\x1\xC2\x1\xC3\x1\xC4\x1\xb0\x1\x1\xb1\x1\x1F",		//	1
	"\xC0\x2\xC1\x2\xC2\x2\xC3\x2\xC4\x2\xb0\x2\x2\xb1\x2\x2F",		//	2
	"\xC0\x3\xC1\x3\xC2\x3\xC3\x3\xC4\x3\xb0\x3\x3\xb1\x3\x3F",		//	3
	"\xC0\x4\xC1\x4\xC2\x4\xC3\x4\xC4\x4\xb0\x4\x4\xb1\x4\x4F",		//	4
	"\xC0\x5\xC1\x5\xC2\x5\xC3\x5\xC4\x5\xb0\x5\x5\xb1\x5\x5F",		//	5
	"\xC0\x6\xC1\x6\xC2\x6\xC3\x6\xC4\x6\xb0\x6\x6\xb1\x6\x6F",		//	6
	"\xC0\x7\xC1\x7\xC2\x7\xC3\x7\xC4\x7\xb0\x7\x7\xb1\x7\x7F",		//	7
	"\xC0\x8\xC1\x8\xC2\x8\xC3\x8\xC4\x8\xb0\x8\x8\xb1\x8\x8F",		//	8
	"\xC0\x9\xC1\x9\xC2\x9\xC3\x9\xC4\x9\xb0\x9\x9\xb1\x9\x9F",		//	9
	"\xC0\xa\xC1\xa\xC2\xa\xC3\xa\xC4\xa\xb0\xa\xa\xb1\xa\xaF"		//	10
};

//	"\xC0\x6\xC1\x6\xC2\x6\xC3\x6\xC4\x6\xb0\x6\x6\xb1\x6\x6F",		//	5
//     c0 06  c1 06  c2 06  c3 06  c4  06 b0 06 06  b1 06  6f
// 06 90 0f 44 d8 06 de  00
// 02 90 04 07 c4 60 90  02 62 90 5e 5e 00 90 02 62 90 5e 5e 90 02 62
GLOBAL	void TEST_UartTransmit()
{
	uint8_t k,l;
	uint16_t Len;
	for (k=1;k<11;k++)
	{
		if (PRESSED == Button[k])
		{
			if ((k>0) && (k<15))
			{
				DISPLAY_WriteDecimal(k,0);
				delay();
				DISPLAY_Blank();
				delay();
			}
			l = 0;
			do {
				DISPLAY_WriteHex(Messages[k][l++] & 0xFF);
				delay();
				delay();
				delay();
			} while ((l <16)&& (PRESSED == Button[k]));
			l = 0;
			do {
				UART_Transmit((uint8_t)(Messages[k][l++] & 0xFF));
			} while (l < 16);
			Len = UART_StartTx();
			DISPLAY_WriteDecimal(Len,0);
		}
	}
}

GLOBAL	void TEST_Pedals()
{
	static __bit WhichPedal	= 0;
	LeftPedal = ADC_Read(0);
	RightPedal = ADC_Read(1);
	if (	((LastLeft+DeadBand) < LeftPedal)
	||		((LastLeft-DeadBand) > LeftPedal)	)
	{
		LastLeft = LeftPedal;
		WhichPedal = 0;
		Display.LED.ExprA = 1;
		Display.LED.ExprB = 0;
	}
	else
	if (	((LastRight+DeadBand) < RightPedal)
	||		((LastRight-DeadBand) > RightPedal)	)
	{
		LastRight = RightPedal;
		WhichPedal = 1;
		Display.LED.ExprA = 0;
		Display.LED.ExprB = 1;
	}
	if (WhichPedal)
		DISPLAY_WriteHex(RightPedal);
	else
		DISPLAY_WriteHex(LeftPedal);
}

void	TEST_KeyPress()
{
	int k;
	for (k=0;k<18;k++)
	{
		if (PRESSED == Button[k])
		{
			if ((k>0) && (k<15))
				DISPLAY_WriteDecimal(k,0);
		}
		else
		{
			DISPLAY_Blank();
		}
		switch(k)
		{
			case 1:
				Display.LED.Key1 = Button[k];
				break;
			case 2:
				Display.LED.Key2 = Button[k];
				break;
			case 3:
				Display.LED.Key3 = Button[k];
				break;
			case 4:
				Display.LED.Key4 = Button[k];
				break;
			case 5:
				Display.LED.Key5 = Button[k];
				break;
			case 6:
				Display.LED.Key6 = Button[k];
				break;
			case 7:
				Display.LED.Key7 = Button[k];
				break;
			case 8:
				Display.LED.Key8 = Button[k];
				break;
			case 9:
				Display.LED.Key9 = Button[k];
				break;
			case 10:
				Display.LED.Key0 = Button[k];
				break;
			case 15:
				if (PRESSED == Button[k])
					DISPLAY_WriteUP();
				break;
			case 16:
				if (PRESSED == Button[k])
					DISPLAY_WriteDOWN();
				break;
			default:
				if (PRESSED == Button[k])
					DISPLAY_WriteERR();
				break;
		}
	}
}
//	SWITCH1		SWITCH2	****	SELECT	NUMBER	VALUE1	VALUE2	DirectSelect	MIDI FUNC	MIDI CHAN	CONFIG
void	TEST_LEDS()
{
	Display.LED.Switch1 = 1;
	delay();
	Display.LED.Switch1 = 0;

	Display.LED.Switch2 = 1;
	delay();
	Display.LED.Switch2 = 0;

	Display.LED.Switch = 1;
	delay();
	Display.LED.Switch = 0;

	Display.LED.Select = 1;
	delay();
	Display.LED.Select = 0;

	Display.LED.Number = 1;
	delay();
	Display.LED.Number = 0;
	
	Display.LED.Value1 = 1;
	delay();
	Display.LED.Value1 = 0;

	Display.LED.Value2 = 1;
	delay();
	Display.LED.Value2 = 0;
	
	Display.LED.DirectBankSelect = 1;	
	delay();
	Display.LED.DirectBankSelect = 0;	

	Display.LED.MidiFunction = 1;
	delay();
	Display.LED.MidiFunction = 0;

	Display.LED.MidiChannel = 1;
	delay();
	Display.LED.MidiChannel = 0;
	
	Display.LED.Config = 1;
	delay();
	Display.LED.Config = 0;

	Display.LED.ExprA = 1;
	delay();
	Display.LED.ExprA = 0;

	Display.LED.ExprB = 1;
	delay();
	Display.LED.ExprB = 0;
}
