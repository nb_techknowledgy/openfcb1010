/* ****************************************************************/
/* File Name    : midi.c                                          */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/


#define PROGRAM_CHANGE(chan)	(0xC0 | (chan & 0x0F)
#define CONTROL_CHANGE(chan)	(0xB0 | (chan & 0x0F)




const uint8_t *Messages[] = {
	"\xC0\0\xC1\0\xC2\0\xC3\0\xC4\0\xb1\0\0",
	"\xC0\1\xC1\1\xC2\1\xC3\1\xC4\1\xb1\1\1",
	"\xC0\2\xC1\2\xC2\2\xC3\2\xC4\2\xb1\2\2",
	"\xC0\3\xC1\3\xC2\3\xC3\3\xC4\3\xb1\3\3",
	"\xC0\4\xC1\4\xC2\4\xC3\4\xC4\4\xb1\4\4",
	"\xC0\5\xC1\5\xC2\5\xC3\5\xC4\5\xb1\5\5",
	"\xC0\6\xC1\6\xC2\6\xC3\6\xC4\6\xb1\6\6",
	"\xC0\7\xC1\7\xC2\7\xC3\7\xC4\7\xb1\7\7",
	"\xC0\8\xC1\8\xC2\8\xC3\8\xC4\8\xb1\8\8",
	"\xC0\9\xC1\9\xC2\9\xC3\9\xC4\9\xb1\9\9"
};
