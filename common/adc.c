/* ****************************************************************/
/* File Name    : adc.c                                           */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
/*
	sdcc -mmcs51 --iram-size 256 --xram-size 65536 --code-size 32768  --nooverlay --noinduction --opt-code-size --verbose --debug -V --std-sdcc89 --model-small   "%f"
*/
#include "common.h"

#define	ADC_CLK		T0
#define	ADC_CS		P1_0
#define	ADC_DO		P1_1
#define	ADC_DI		P1_2

__near static	uint8_t	Value;
__near static	uint8_t	k;

#pragma callee_saves ClockADC
LOCAL	void ClockADC() __naked
{
	T0 = 1;
	__NOP;
	T0 = 0;
	__NOP;
	__RETURN;
}

//#define NO_ASM_OPTIMISATION
//	__bit	Chan;
GLOBAL	uint8_t ADC_Read(__bit Chan)
{
	//	CHAN 0 -- send 110   CHAN 1 -- send 111
	Value = 0;
	ADC_DO = 1;
	ADC_CLK = 0;
	__NOP;
	ADC_CS = 0;
	ADC_DI = 1;
	ADC_DO = 1;
	__NOP;
	ClockADC();
	ClockADC();
	ADC_DI = Chan;
	ClockADC();
	k = 7;
	do	{
		ClockADC();
	}	while (--k);
	k = 8;
#ifdef NO_ASM_OPTIMISATION
	do	{
		ClockADC();
		Value = (Value << 1);
		Value = Value | ADC_DO;
	}	while (--k);
#else
	__asm__("mov A, _Value");
	do	{
		ClockADC();
		__asm
			setb	C
			jb	_P1_1, 00100$
			clr	C
		00100$:
			rrc	A
		__endasm;
	}	while (--k);
	__asm__("mov _Value,	A");
#endif
	ADC_CS = 1;
	return Value;
}

