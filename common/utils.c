/* ****************************************************************/
/* File Name    : utils.c                                         */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include "common.h"

void delay()
{
#ifdef SIMULATOR
extern	__idata	volatile	int8_t		Digit;
	int x = 250;
	__NOP;
	while (Digit != 2);
#else
	int x = 25000;
	while(x--);
#endif
}
