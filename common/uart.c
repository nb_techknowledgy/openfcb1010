/* ****************************************************************/
/* File Name    : uart.c                                          */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include "common.h"
#include "utils.h"
#include "display.h"

//	Timer 1 is used for baud rate control
//	There 32Kb of RAM -- so large buffers are OK

#define		BUFFER_SIZE		(4096)
#define		BUFFER_MASK		(BUFFER_SIZE-1)

__xdata	__at (0x9000)	uint8_t		ReceiveBuffer[BUFFER_SIZE];
__xdata	__at (0xA000)	uint8_t		TransmitBuffer[BUFFER_SIZE];
__sbit		StartTx;

uint16_t	TxHead,TxTail;
uint16_t	RxHead,RxTail;
uint16_t	TxLen = 0;

GLOBAL void	UART_Init()
{
	TR1 = 0;						//	stop Timer 1
	TH1 = 0xFF;						//	set timer registers
	TL1 = 0x0FF;
	TMOD = (TMOD &0x0F) | 0x20;		//	Timer 1 set to 8 bit auto reload
	SCON = 0x50;					//	Serial port set to 8 bit
	PCON &= ~0x80;					//	baud rate 31250 set by Timer 1
	TR1	= 1;						//	start Timer 1
	REN	= 1;						//	enable Uart
	RI = 0;							//	clear receive interrupt
	PS = 1;							//	set uart as highest interrupt
	PT2 = 0;						//	lower Timer 2 interrupt level
	PT0 = 0;						//	lower Timer 0 interrupt level
	TxHead = 10;
	TxTail = 10;
	RxHead = 10;
	RxTail = 10;
	StartTx = 0;
	ES = 1;
}

GLOBAL	void UART_ISR (void) __interrupt 4
{
	uint16_t tmp;
	Display.LED.Config = 1;
	if (RI)
	{
		Display.LED.Switch1 = 1;
		//	=======================================
		//	Handle incoming bytes from Uart
		//	=======================================
		tmp = ((RxHead+1) & BUFFER_MASK);
		RI = 0;
		if (RxTail != tmp)
		{
			ReceiveBuffer[RxHead] = SBUF;
			RxHead++;
			RxHead &= BUFFER_MASK;
		}
		//	=======================================
	}
	if (TI) Display.LED.Switch2 = 1;
	if (StartTx) Display.LED.DirectBankSelect = 1;
	if (TI || (StartTx == 1))
	{
		//	=======================================
		//	Send byte to Uart
		//	=======================================
		
		Display.LS[1] = ASC_T;
		Display.LS[0] = ONE;
		TI = 0;								//	clear interrupt flag
		if (TxHead != TxTail)				//	is there data in the buffer ?
		{
			SBUF = TransmitBuffer[TxTail];	//	YES -- send a byte
			StartTx = 0;					//	Clear flag to say we HAVE started
			TxTail++;
			TxTail &= BUFFER_MASK;
			TxLen--;
		}
		//	=======================================
		Display.LS[1] = BLANK;
		Display.LS[0] = BLANK;		
	}
}

GLOBAL	uint16_t	UART_ReceiveLen()
{
	if (RxTail > RxHead)
	{
		return (RxHead-(RxTail+BUFFER_SIZE));
	}
	else	
		return (RxHead-RxTail);
}

GLOBAL	uint16_t	UART_TransmitLen()
{
	if (TxTail > TxHead)
	{
		return (TxHead-(TxTail+BUFFER_SIZE));
	}
	else	
		return (TxHead-TxTail);
}

GLOBAL	uint8_t	UART_Receive()
{
	if (RxHead != RxTail)
	{
		uint8_t Byte;
		Byte = ReceiveBuffer[RxTail];
		RxTail++;
		RxTail &= BUFFER_MASK;
		return Byte;
	}
	return 0;
}

GLOBAL	uint16_t UART_StartTx()
{
	if (TxHead != TxTail)
	{
		Display.LED.Switch = 1;
		SBUF = TransmitBuffer[TxTail];	//	YES -- send a byte
		TxTail++;
		TxTail &= BUFFER_MASK;
		TxLen--;
	}
	return(TxHead-TxTail);
}
	
GLOBAL	uint8_t	UART_Transmit(uint8_t Byte)
{
	// TODO -- Add check for buffer full !!
	static uint8_t LocalSent = 0;
	TransmitBuffer[TxHead] = Byte;
	TxHead++;
	TxHead &= BUFFER_MASK;
	TxLen++;
	return 1;
}

GLOBAL	void UART_DumpRxBuff()
{
	uint16_t Len = UART_ReceiveLen();
	uint16_t k = 0;
	do
	{
		UART_Transmit(ReceiveBuffer[k++]);
	} while (--Len);
}
