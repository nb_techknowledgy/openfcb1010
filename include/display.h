/* ****************************************************************/
/* File Name    : display.h                                       */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include <stdint.h>
#include "displayhw.h"

// These are the i/o data latches and are WRITE ONLY
// and must be declared volatile to ensure they are
// always written to and not optimized out
__xdata	__at	(0) volatile	uint8_t	CS_LED_7;
__xdata	__at	(1) volatile	uint8_t	CS_LED_8;
__xdata	__at	(6) volatile 	uint8_t	RST_273;
__xdata	__at	(7) volatile 	uint8_t	RST_273_2;

extern __idata	volatile	tDISPLAY	Display;		//	Contains the actual LED display data
extern	__idata	volatile	int8_t		Digit;			//	Last value written to CS_LED7
														//	(Current Digit (LSxx) being driven)
void	DISPLAY_Init();
void	DISPLAY_Update();

// The display would normally be 0 to 127 decimal but we can do 0 to 199 decimal
void	DISPLAY_WriteDecimal(int16_t DecValue, int8_t LeadingZero);

// AND we can do 000 - 1FF in hex !!
void	DISPLAY_WriteHex(uint16_t HexValue);
void	DISPLAY_WriteUP();
void	DISPLAY_WriteDOWN();
void	DISPLAY_WriteERR();

void	DISPLAY_Blank();
void	DISPLAY_ClearLEDS();

#endif	/* ifndef  __DISPLAY_H__ */
