/* ****************************************************************/
/* File Name    : buttons.c                                       */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include "common.h"
#include "buttons.h"

#ifdef TEST_MODE
#include "display.h"
#define BUTTON_TEST(x,y)	Display.LED.x = y;
#else
#define BUTTON_TEST(x,y)	while (0)	{;}
#endif

#ifdef SIMULATOR
#define	CS_PER		P2
__xdata	__at	(6) volatile 	uint8_t	RST_273;
__xdata	__at	(7) volatile 	uint8_t	RST_273_2;
#else
__xdata	__at	(5) volatile 	uint8_t	CS_PER;
#endif

		volatile	uint8_t	BUTTONS_CS_PER = 0;	// 	Last value written to keypad scan latch
		volatile	uint8_t	KeyColumn = 0;
__idata	volatile	uint8_t	CurrentKeyState[20];
__idata	volatile	uint8_t	PreviousKeyState[20];
__idata	volatile	uint8_t	KeyHeldForCount[20];
__idata				uint8_t	Button[20];

const uint8_t KeyMap[] = { DOWN, 1, 2, 3, 4, 5, UP, 6, 7, 8, 9, 10 };

//#define CHECK_HKEYS
void BUTTONS_Update()
{
	BUTTONS_CS_PER &= 3;						//	Zero all but bits 0/1
	BUTTONS_CS_PER |= (4 << KeyColumn);		//	Set each button column high
	CS_PER = BUTTONS_CS_PER;					//	Write out local copy to the latch
	KeyColumn++;
	if (KeyColumn > 5)
		KeyColumn = 0;
	Button[KeyMap[KeyColumn  ]] = P1_6;		//	Get state of KEY_H2 -- button pressed ?
	Button[KeyMap[KeyColumn+6]] = P1_5;		//	Get state of KEY_H1 -- button pressed ?
#ifdef CHECK_HKEYS
	if (PRESSED == Button[KeyColumn  ])
	{
		Display.LED.Switch1 = 1;
	}
	if (PRESSED == Button[KeyColumn+6])
	{
		Display.LED.Switch2 = 1;
	}
#endif
}

