/* ****************************************************************/
/* File Name    : FCB1010.c                                       */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include "common.h"
#include "adc.h"
#include "buttons.h"
#include "display.h"
#include "nvram.h"
#include "pedals.h"
#include "timers.h"
#include "uart.h"
#include "utils.h"

#include "test.h"

#define MAX_BANK	(10)
#define MAX_PRESETS	(100)

//--------------------------------------------------------------
__idata volatile	uint8_t	CurrentBank;
__idata volatile	uint8_t	CurrentPreset;

__idata 			uint8_t	preset;
__idata				uint8_t	DirectSelect;
//--------------------------------------------------------------

NVRAM(MAX_PRESETS,nvram);

GLOBAL	void TIMERS_Timer2_ISR (void) __interrupt 5 __using 2;
//GLOBAL	void UART_ISR (void) __interrupt 4;

void SelectPreset(uint8_t Preset, uint8_t Action)
{
	if (PRESSED == Action)
	{
		CurrentPreset = Preset;
#ifdef COMMENTED
		if (Preset == LastPreset)
		{
			if (
			// Already sent PC and CC 1st
			/* pseudo code
			 * if (toggle)
			 * {
			 * 		send alternate CC
			 * 		set LED according to CC state
			 */
		}
		else
		{
			for (pc=0;pc<4;pc++)
			{
				if(ProgramChange[Preset])
				{
					;
				}
			}
		}
		// Do STOMP/CC Toggle coding here
		if (ControlChange1[Preset])
		{
			;
		}
		if (ControlChange2[Preset])
		{
			;
		}
#endif
	}
}

void ProcessKey(uint8_t key, uint8_t action)
{
#ifdef TEST_MODE
	if (PRESSED == action)
		DISPLAY_WriteDecimal(key,0);
#else
		switch (key)
		{
			case 1:			// Button 1
			case 2:			// Button 2
			case 3:			// Button 3
			case 4:			// Button 4
			case 5:			// Button 5
			case 6:			// Button 6
			case 7:			// Button 7
			case 8:			// Button 8
			case 9:			// Button 9
			case 10:		// Button 10 ( 0 )
//			case TOE_1:
//			case TOE_2:
//			case HEEL_1:		//
//			case HEEL_2:
				SelectLED(
//				SelectPreset((CurrentBank*10)+key,action);
				break;
			case UP:		// Button 'UP'
				if(CurrentBank < MAX_BANK)
					CurrentBank++;
					DisplayDecimal(CurrentBank,1);
				break;
			case DOWN:		// Button 'DOWN'
				if(CurrentBank > 0)
					CurrentBank--;
					DisplayDecimal(CurrentBank,1);
				break;
		}
#endif
}

uint8_t	GetKeyPress()
{
	uint8_t	key;
	for (key=1;key<LAST_KEY;key++)
	{
		if( Button[key] == PRESSED)
			DISPLAY_WriteDecimal(key,0);
#ifdef COMMENTED
		CurrentKeyState[key] = Button[key];
		if (PreviousKeyState[key] == CurrentKeyState[key])
		{
			if (PRESSED == CurrentKeyState[key])
			{
				ProcessKey(key,HELD);						// key held
				KeyHeldForCount[key]++;
				if ((DOWN == key) && (KeyHeldForCount[key] > SETUP_TIME))
				{
					//PresetSetup();
				}
				if ((DOWN == key) && (KeyHeldForCount[key] > DEBOUNCE_TIME))
				{
					if (DirectSelect)
					{
						if (0 == preset)
						{
							preset = key*10;
						}
						else
						{
							preset += key;
							SelectPreset(preset,PRESSED);
						}
					}
					else	// NORMAL OPERATION -- BANK MODE
					{
						ProcessKey(key,CurrentKeyState[key]);			// key pressed/released event
					}
				}
			}
			if (RELEASED == CurrentKeyState[key])
			{
				KeyHeldForCount[key] = 0;
				// don't care if steady state released !
			}
		}
		else
		{
			PreviousKeyState[key] = CurrentKeyState[key];	// register new state
		}
#endif
	}
	return key;
}
#ifdef COMMENTED
void ReadNVram()
{
#ifdef I2c_READY
	// load all nvram into a struct in RAM.
	char *pChar = (char*)&nvram;
	i2c_ReadAddr(0);
	for (k=0;k< sizeof(nvram);k++)
	{
		pChar = i2c_ReadByte();
		pChar+k;
	}
#endif
}

void WriteNVram()
{
#ifdef I2c_READY
	char *pChar = (char*)&nvram;
	i2c_Address(0);
	for (k=0;k< sizeof(nvram);k++)
	{
		i2c_WriteByte(pChar);
		pChar+k;
	}
#endif
}

void InitVars()
{
	ReadNVram();
}
#endif

extern uint16_t	TxLen;
extern __xdata	__at (0x9000)	uint8_t		ReceiveBuffer[];

void main(void)
{
	uint8_t k;
	P1 = 255;
	DISPLAY_Init();
	DISPLAY_ClearLEDS();
	TIMERS_Timer2_Init(400);
	DISPLAY_Blank();
	UART_Init();
	//	Do a startup countdown .. to confirm operation
	k = 5;
	do
	{
//		DISPLAY_WriteDecimal(k,0);
		DISPLAY_WriteHex(0xC0+k);
		delay();
	}
	while (--k);
	DISPLAY_Blank();
	//	Entering main control loop
	while(1)
	{
		uint16_t RxLen;
//		TEST_LEDS();
#ifdef DO_PEDALS
		TEST_Pedals();
		delay();
		delay();
		DISPLAY_Blank();
#endif
#ifdef DO_BUTTONS
		TEST_KeyPress();
//		delay();
//		delay();
		DISPLAY_ClearLEDS();
#endif
		TEST_UartTransmit();
#ifdef TEST_RECEIVE
		RxLen = UART_ReceiveLen();
//		RxLen = RxLen /10;
		DISPLAY_WriteDecimal(RxLen,0);
		delay();
		if (PRESSED == Button[15])
		{
			char ch;
			do
				ch = UART_Receive();
			while (ch !=0);
		}
		if (PRESSED == Button[16])
		{
			DISPLAY_WriteDOWN();
			delay();
			delay();
			DISPLAY_Blank();
			UART_DumpRxBuff();
//			do
//			{
//				DISPLAY_WriteDecimal(TxLen,0);
//				delay();
//			} while (1);
		}
#endif
	}
}

#ifdef COMMENTED
void PresetSeup()
{
	int ConfigItem = 0;
	while(1)
	{
		uint8_t key;
		for (key=1;key<LAST_KEY;key++)
		{
			CurrentKeyState[key] = Button[key];
			if (PreviousKeyState[key] == CurrentKeyState[key])
			{
				if (PRESSED == CurrentKeyState[key])
				{
					KeyHeldForCount[key]++;
					if (KeyHeldForCount[key] > CONFIRM_COUNT)
					{
						switch (key)
						{
							case 1:
							case 2:
							case 3:
							case 4:
							case 5:
							case 6:
							case 7:
							case 8:
							case 9:
							case 10:
								// toggle the control enable
								break;
							case UP:
								// Effectively pressed Enter to accept change
								break;
							case DOWN:
								// cancel change
								break;
							default:
								break;
						}
					}
					if ((DOWN == key) && (KeyHeldForCount[key] > SETUP_TIME))
					{
						// SAVE TO NVRAM
						return;
					}
				}
				if (RELEASED == CurrentKeyState[key])
				{
					if (KeyHeldForCount[key] < CONFIRM_COUNT)
					{
						switch (key)
						{
							case 1:
							case 2:
							case 3:
							case 4:
							case 5:
							case 6:
							case 7:
							case 8:
							case 9:
							case 10:
								// select item to change setting
								break;
							default:
								break;
						}
					}
					KeyHeldForCount[key] = 0;
					// don't care if steady state released !
				}
			}
			else
			{
				PreviousKeyState[key] = CurrentKeyState[key];	// register new state
				if (DirectSelect)
				{
					if (0 == preset)
					{
						preset = key*10;
					}
					else
					{
						preset += key;
						SelectPreset(preset,PRESSED);
					}
				}
				else	// NORMAL OPERATION -- BANK MODE
				{
					ProcessKey(key,CurrentKeyState[key]);			// key pressed/released event
				}
			}
		}
	}
}
#endif
