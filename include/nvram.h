/* ****************************************************************/
/* File Name    : nvram.h                                         */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __NVRAM_H
#define __NVRAM_H

#include <stdint.h>

typedef union tagProgChange
{
	union {
		struct {
			uint8_t	Enable:1;		//	enabled when bit clear
			uint8_t Value:7;		//	Program change number 0-127
		};
		uint8_t Byte;
	}
	;
} tPROG_CHANGE, *pPROG_CHANGE;

typedef struct tagControlChange
{
	union {
		struct	{
			uint8_t Enable:1;		//	enabled when bit clear
			uint8_t	Controller:7;	//	Controller number 0-127
		};
		uint8_t CC;
	};
	union {
		struct	{
			uint8_t	Relay:1;		//	Amp relay active when bit clear
			uint8_t	Value:7;		//	Controller value to send when active
		};
		uint8_t Val;
	};
} tCTRL_CHANGE,*pCTRL_CHANGE;

typedef struct tagExpressionPedal
{
	union {
		struct	{
			uint8_t Enable:1;		//	enabled when bit clear
			uint8_t	Controller:7;	//	Controller number 0-127
		};
		uint8_t CC;
	};
	union {
		struct {
			uint8_t Switch:1;		//	Heel switch function -- enabled when bit clear
			uint8_t Value:7;		//	Value when pedal rocked all the way back ( heel )
		} Heel;
		uint8_t	HeelValue;
		struct {
			uint8_t Switch:1;		//	Toe switch function -- enabled when bit clear
			uint8_t Value:7;		//	Value when pedal pressed all the way forward ( toe )
		} Toe;
		uint8_t	ToeValue;
	};
} tEXPR_PEDAL,*pEXPR_PEDAL;

typedef union tagEnables
{
	uint8_t	Enables;
	struct
	{
		uint8_t	EnableExp1:1;
		uint8_t	EnableToe1:1;
		uint8_t	EnableHeel1:1;
		uint8_t	EnableExp2:1;
		uint8_t	EnableToe2:1;
		uint8_t	EnableHeel2:1;
		uint8_t	StompMode:1;
		uint8_t	Momentary:1;
	};
} tENABLES, *pENABLES;

typedef struct tagPRESETS
{
	tPROG_CHANGE	PC[5];
	tCTRL_CHANGE	CC[2];
	tEXPR_PEDAL		Pedal[2];
	uint8_t			Note;
} tPRESETS,*pPRESETS;

#define NVRAM(Count,Name)	typedef struct { tPRESETS Presets[Count]; tENABLES Enables[Count]; } Name

#endif
