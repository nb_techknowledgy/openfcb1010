/* ****************************************************************/
/* File Name    : timers.c                                        */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include "common.h"
#include "timers.h"
#include "display.h"
#include "buttons.h"

extern __xdata	__at	(5) volatile 	uint8_t	CS_PER;

GLOBAL	void TIMERS_Timer2_Init(uint16_t ClockTicks)
{
	/*	Set Timer2 for 16-bit auto-reload.
		The timer counts to 0xFFFF, overflows,
		is reloaded, and generates an interrupt.
	*/
//	T2CON = 0;		//	x80;                /* 10000000 */
	/*	Set the reload values to be ClockTicks clocks.	*/
	RCAP2L = (65536UL-ClockTicks);
	RCAP2H = (65536UL-ClockTicks) >> 8;

	TL2 = RCAP2L;
	TH2 = RCAP2H;

	ET2 = 1;                      /* Enable Timer 2 Interrupts */
	TR2 = 1;                      /* Start Timer 2 Running */
	EA = 1;                       /* Global Interrupt Enable */
}

GLOBAL	void TIMERS_Timer2_ISR (void) __interrupt 5 __using 2
{
	TF2 = 0;							//	Clear the interrupt request
	DISPLAY_Update();
	BUTTONS_Update();
}
