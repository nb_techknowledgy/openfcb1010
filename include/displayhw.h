/* ****************************************************************/
/* File Name    : displayhw.h                                     */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __DISPLAYHW_H
#define __DISPLAYHW_H

/*
 *		 ---		A
 * 	F	|	|		B
 * 		 ---		G
 * 	E	|	|		C
 * 		 ---		D
 */

// LED Order
//	SWITCH1		SWITCH2	****	SELECT	NUMBER	VALUE1	VALUE2	DirectSelect	MIDI FUNC	MIDI CHAN	CONFIG
/*
 * SWITCH1
 * SWITCH2
 * ****
 * SELECT
 * NUMBER
 * VALUE1
 * VALUE2
 * DirectSelect
 * MIDI FUNC
 * MIDI CHAN
 * CONFIG
 */
// For LS0,1,2
#define SEG_DP				(1)
#define SEG_G				(1 << 1)
#define SEG_F				(1 << 2)
#define SEG_E				(1 << 3)
#define SEG_D				(1 << 4)
#define SEG_C				(1 << 5)
#define SEG_B				(1 << 6)
#define SEG_A				(1 << 7)
#define SEG_MINUS			(SEG_A)
#define SEG_PLUS			(SEG_A | SEG_G)

//-------------------------------------------------------------------------
#define ZERO		(SEG_A + SEG_B + SEG_C + SEG_D + SEG_E + SEG_F        )
#define ONE			(        SEG_B + SEG_C                                )
#define TWO			(SEG_A + SEG_B +         SEG_D + SEG_E +         SEG_G)
#define THREE		(SEG_A + SEG_B + SEG_C + SEG_D +                 SEG_G)
#define FOUR		(        SEG_B + SEG_C                 + SEG_F + SEG_G)
#define FIVE		(SEG_A +         SEG_C + SEG_D +       + SEG_F + SEG_G)
#define SIX			(SEG_A +         SEG_C + SEG_D + SEG_E + SEG_F + SEG_G)
#define SEVEN		(SEG_A + SEG_B + SEG_C                                )
#define EIGHT		(SEG_A + SEG_B + SEG_C + SEG_D + SEG_E + SEG_F + SEG_G)
#define NINE		(SEG_A + SEG_B + SEG_C +                 SEG_F + SEG_G)
//-------------------------------------------------------------------------
#define HEX_A		(SEG_A + SEG_B + SEG_C +         SEG_E + SEG_F + SEG_G)
#define HEX_B		(                SEG_C + SEG_D + SEG_E + SEG_F + SEG_G)
#define HEX_C		(SEG_A +                 SEG_D + SEG_E + SEG_F        )
#define HEX_D		(        SEG_B + SEG_C + SEG_D + SEG_E +         SEG_G)
#define HEX_E		(SEG_A +                 SEG_D + SEG_E + SEG_F + SEG_G)
#define HEX_F		(SEG_A +                         SEG_E + SEG_F + SEG_G)
//-------------------------------------------------------------------------
#define	ASC_N		(                SEG_C +         SEG_E +         SEG_G)
#define	ASC_P		(SEG_A + SEG_B +                 SEG_E + SEG_F + SEG_G)
#define	ASC_R		(                                SEG_E +         SEG_G)
#define ASC_T		(                        SEG_D + SEG_E + SEG_F + SEG_G)
#define ASC_U		(        SEG_B + SEG_C + SEG_D + SEG_E + SEG_F        )
//-------------------------------------------------------------------------
#define LAMP_TEST	(0xFF)
#define DASH		(SEG_G)
#define	BLANK		(0)
//-------------------------------------------------------------------------

#define LETTER_A	(0xA)
#define LETTER_B	(0xB)
#define LETTER_C	(0xC)
#define LETTER_D	(0xD)
#define LETTER_E	(0xE)
#define LETTER_F	(0xF)
#define LETTER_N	(16)
#define LETTER_P	(17)
#define LETTER_R	(18)
#define LETTER_T	(19)
#define LETTER_U	(20)

typedef union tagDisplay
{
		uint8_t LS[6];
		struct {
			union {
				struct {
					uint8_t	DP:1;		// LSB of byte
					uint8_t	G:1;
					uint8_t	F:1;
					uint8_t	E:1;
					uint8_t	D:1;
					uint8_t	C:1;
					uint8_t	B:1;
					uint8_t A:1;		// MSB of byte
				} Seg;
				union {
					uint8_t Digit;
					uint8_t byte;
				};
			} LS0;
			union {
				struct {
					uint8_t	DP:1;		// LSB of byte
					uint8_t	G:1;
					uint8_t	F:1;
					uint8_t	E:1;
					uint8_t	D:1;
					uint8_t	C:1;
					uint8_t	B:1;
					uint8_t A:1;		// MSB of byte
				} Seg;
				uint8_t Digit;
				uint8_t byte;
			} LS1;
			union {
				struct {
					uint8_t	DP:1;		// LSB
					uint8_t	G:1;
					uint8_t	_spare:3;
					uint8_t	C:1;
					uint8_t	B:1;
					uint8_t	plus:1;		// MSB
				} Seg;
				union {
					uint8_t Digit;
					uint8_t byte;
				};
			} LS2;
//			===================================================================
			union {
				struct {
//			===================================================================
					union	{
						struct {
							uint8_t	MidiFunction:1;		// LSB of byte
							uint8_t	MidiChannel:1;
							uint8_t	ExprB:1;
							uint8_t	ExprA:1;
							uint8_t	Switch1:1;
							uint8_t	Switch2:1;
							uint8_t	Key5:1;
							uint8_t Key4:1;		// MSB of byte
						};
						uint8_t	KEY_0_9;
						uint8_t LS3;
					} ;
//			===================================================================
					union	{
						struct {
							uint8_t	Key3:1;		// LSB of byte
							uint8_t	Key2:1;
							uint8_t	Key1:1;
							uint8_t	Key0:1;
							uint8_t	Key9:1;
							uint8_t	Key8:1;
							uint8_t	Key7:1;
							uint8_t	Key6:1;		// MSB of byte
						} ;
						uint8_t	KEY_1_8;
						uint8_t LS4;
					};
//			===================================================================
					union	{
						struct {
							uint8_t	DirectBankSelect:1;		// LSB of byte
							uint8_t	Value2:1;
							uint8_t	Value1:1;
							uint8_t	Number:1;
							uint8_t	Select:1;
							uint8_t	Switch:1;
							uint8_t	Config:1;
							uint8_t _spare:1;		// MSB of byte
						};
						uint8_t LS5;
					};
				} LED;
//			===================================================================
			};
		};
} tDISPLAY, *pDISPLAY;
#endif
