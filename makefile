#==================================================
# FCB1010 (sdcc) Makefile
#==================================================
# Author       : Andrew Carney
# Date         : 16th February 2021
# Version      : First Version for Behringer FCB1010 pedals
# Copyright(c) 2021  Andrew Carney
# License	: GPL v3.0 or later
# **********************************************************************
# Modification :
# **********************************************************************
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>
# **********************************************************************
TARGET			=	FCB1010
#==================================================
#  Command definitions
#==================================================
LD			=	sdld
CC			=	sdcc
ASM			=	sdas8051
#==================================================
#	C Compiler command line options
#==================================================
CPU			=	-mmcs51
MODEL		=	--model-small
#	OPTIMISE	=	--opt-code-size
RAM_SIZE	=	32768
RAM_START	=	32768
ROM_SIZE	=	32768
# -Wl /usr/share/sdcc/lib/large/*
CFLAGS		=	$(CPU) --compile-only --std-sdcc99 $(MODEL)
LD_FLAGS	=	--iram-size	25				\
				--xram-loc	$(RAM_START)	\
				--xram-size	$(RAM_SIZE)		\
				--code-size	$(ROM_SIZE)
#	=================================================
#	Manually link instead of letting sdcc do it for us
#	Most options derived from the linker file generated
#	by sdcc in link mode
SDLD_FLAGS	=	-n	-I 0x0100							\
				-C $(ROM_SIZE)							\
				-X $(RAM_SIZE)							\
				-b XSEG=$(RAM_START)					\
				-b PSEG=$(RAM_START)					\
				-b HOME=0x0000							\
				-b ISEG=0x0000							\
				-b BSEG=0x0000							\
				-k /usr/bin/../share/sdcc/lib/small		\
				-k /usr/share/sdcc/lib/small			\
				-l mcs51 -l libsdcc -l libint			\
				-l liblong -l libfloat
#	=================================================
INCLUDES		=	-I./include
SYS_INCLUDE		=	-I/usr/share/sdcc/include	\
					-I/usr/share/sdcc/include/mcs51
SOURCE_DIR		=	./src
COMMON_DIR		=	./common
INCLUDE_DIR		=	./include
TARGET_DIR		=	./obj
#==================================================
SOURCES			=	$(SOURCE_DIR)/FCB1010.c		\
					$(COMMON_DIR)/adc.c			\
					$(COMMON_DIR)/buttons.c		\
					$(COMMON_DIR)/display.c		\
					$(COMMON_DIR)/pedals.c		\
					$(COMMON_DIR)/test.c		\
					$(COMMON_DIR)/timers.c		\
					$(COMMON_DIR)/uart.c		\
					$(COMMON_DIR)/utils.c
#==================================================
OBJECTS			=	$(SOURCES:.c=.rel)
ASM_OBJECTS		=	$(SOURCES:.c=.asm)
LST_OBJECTS		=	$(SOURCES:.c=.lst)
SYM_OBJECTS		=	$(SOURCES:.c=.sym)
RST_OBJECTS		=	$(SOURCES:.c=.rst)
INTERMEDIATES	=	$(ASM_OBJECTS)		\
					$(LST_OBJECTS)		\
					$(SYM_OBJECTS)		\
					$(RST_OBJECTS)
CLEAN_OBJECTS	=	$(OBJECTS)			\
					$(INTERMEDIATES)
DEPENDS			=	depends.inc
#==================================================
#	Dependency rules start here .....
#==================================================
%.asm : %.c
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $(@D)/ $<

%.rel : %.c
	$(CC) $(CFLAGS) $(INCLUDES) $(LD_FLAGS) -o $(@D)/ $<

#==================================================
#	Target Definitions
#==================================================
$(TARGET).hex	:	$(TARGET_DIR)$(TARGET).ihx
	@echo "========= PACK IHX ==========="
	packihx ./obj/FCB1010.ihx > FCB1010.hex
	@echo "=============================="

$(TARGET_DIR)$(TARGET).ihx	:	$(OBJECTS)
	@echo "========== LINKING ==========="
	$(RM) $(INTERMEDIATES)
#	Manually link instead of letting sdcc do it for us
#	$(CC) $(LD_FLAGS) $^
	$(LD) $(SDLD_FLAGS) -i ./obj/$(TARGET).ihex $^
	@echo "=============================="

$(OBJECTS)	:	$(DEPENDS)
$(DEPENDS)	:	$(SOURCES)
	touch $(DEPENDS)
	makedepend	-Y -f $(DEPENDS) $(INCLUDES) $(SYS_INCLUDE) -o.rel $^

#==================================================
#	General rules start here .....
#==================================================
.PHONY: depend clean
all:	$(TARGET).hex $(DEPENDS)

eeprom:	$(TARGET) $(DEPENDS)
	minipro -p AT28C256@SOIC28 -f ihex -w FCB1010.hex

eeprom8K: $(TARGET) $(DEPENDS)
	minipro -p AT28C64B -f ihex -w FCB1010.hex

clean:
	$(RM) $(CLEAN_OBJECTS)
	$(RM) $(TARGET).hex $(TARGET_DIR)/*

cleanall:
	$(RM) $(CLEAN_OBJECTS) $(TARGET).hex $(TARGET_DIR)/$(TARGET).* 
	$(RM) $(TARGET).hex $(DEPENDS) $(DEPENDS).bak

# Only use the 'depend' target if it doesn't happen automatically
depend: $(SOURCES)
	touch depends.inc
	makedepend	-Y -f $(DEPENDS) $(INCLUDES) -o.rel $^

include $(DEPENDS)
