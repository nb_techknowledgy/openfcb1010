/* ****************************************************************/
/* File Name    : display.c                                       */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 10 March 2021                                   */
/* Version      : First Version for Behringer FCB1010 pedals      */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include "common.h"
#include "display.h"

#ifdef SIMULATOR

#define CS_LED_7	P0
#define CS_LED_8	P1
#define	CS_PER		P2
__xdata	__at	(6) volatile 	uint8_t	RST_273;
__xdata	__at	(7) volatile 	uint8_t	RST_273_2;
#else
__xdata	__at	(5) volatile 	uint8_t	CS_PER;
#endif

const uint8_t Digits[] = {	ZERO,	ONE,	TWO,	THREE,
							FOUR,	FIVE,	SIX,	SEVEN,
							EIGHT,	NINE,	HEX_A,	HEX_B,
							HEX_C,	HEX_D,	HEX_E,	HEX_F,
							ASC_N,	ASC_P,	ASC_R,	ASC_U
						};

// These variables are used by interrupt routines
//--------------------------------------------------------------
__idata	volatile	tDISPLAY	Display;		//	Contains the actual LED display data
__idata	volatile	int8_t		Digit;			//	Last value written to CS_LED7
												//	(Current Digit (LSxx) being driven)
//--------------------------------------------------------------
GLOBAL	void	DISPLAY_Init() __using 0
{
#ifdef SIMULATOR
	P0 = 0;
	P1 = 255;
#endif
	Display.LS[0] = BLANK;
	Display.LS[1] = BLANK;
	Display.LS[2] = BLANK;
	Display.LS[3] = BLANK;
	Display.LS[4] = BLANK;
	Display.LS[5] = BLANK;
	RST_273 = 0;
	RST_273_2 = 0;
}

GLOBAL	void DISPLAY_Update()	__naked
{
	CS_LED_8 = 255;						//	turn off all SEGMENTS
#ifdef SIMULATOR
	CS_LED_7 = ~(1 << Digit);			//	then set LED column drive
	CS_LED_8 = ~(Display.LS[Digit]);	//	then sets SEGMENTS register
#else
	CS_LED_7 = (1 << Digit);			//	then set LED column drive
	CS_LED_8 = ~(Display.LS[Digit]);	//	then sets SEGMENTS register
#endif
	Digit++;							//	next digit
	if (Digit > 5)						//	beyond last digit
		Digit = 0;						//	reset back to first
	__RETURN;
}

// The display would normally be 0 to 127 decimal but we can do 0 to 199 decimal
// AND we can do 000 - 1FF in hex !!
GLOBAL	void DISPLAY_WriteDecimal(int16_t DecValue, int8_t LeadingZero)
{
	uint8_t		Negative = 0;
	uint16_t	Index = 0;
	if (DecValue < 0)
	{
		Negative = 1;
		DecValue *= -1;
	}
	if (DecValue > 199)
	{
		Display.LS[0] = DASH;
		Display.LS[1] = DASH;
		Display.LS[2] = DASH;
	}
	Index = DecValue / 10;
	Display.LS[1] = Digits[( Index    % 10)];
	Display.LS[0] = Digits[( DecValue % 10)];
	if (DecValue > 99)
		Display.LS[2] = Digits[1];	// can only be either BLANK or '1'
	if (0 == LeadingZero)
	{
		if (DecValue < 10)
		{
			Index = 1;
		}
		else
		{
			Index = 2;
		}
		if (Display.LS[Index] == ZERO)
			Display.LS[Index] = BLANK;
		if(Negative)
		{
			if (0 == Display.LS[1])
				Display.LS[1] = DASH;
			else
				Display.LS[2] |= DASH;
		}
	}
	else
	{
		if(Negative)
		{
			Display.LS[2] |= DASH;
		}
	}
}

GLOBAL	void DISPLAY_WriteHex(uint16_t HexValue)
{
	Display.LS[0] = Digits[( HexValue       & 15)];
	Display.LS[1] = Digits[((HexValue >> 4) & 15)];
	if (HexValue > 255)
		Display.LS[2] = Digits[1];
	else
		Display.LS[2] = BLANK;
}

GLOBAL	void DISPLAY_WriteUP()
{
	Display.LS[0] = Digits[LETTER_P];
	Display.LS[1] = Digits[LETTER_U];
	Display.LS[2] = BLANK;
}

GLOBAL	void DISPLAY_WriteDOWN()
{
	Display.LS[0] = Digits[LETTER_N];
	Display.LS[1] = Digits[LETTER_D];
	Display.LS[2] = BLANK;
}

GLOBAL	void DISPLAY_WriteERR()
{
	Display.LS[0] = Digits[LETTER_R];
	Display.LS[1] = Digits[LETTER_E];
	Display.LS[2] = BLANK;
}


GLOBAL	void DISPLAY_Blank()
{
	Display.LS[0] = BLANK;
	Display.LS[1] = BLANK;
	Display.LS[2] = BLANK;
}

GLOBAL	void DISPLAY_ClearLEDS()
{
	Display.LS[3] = BLANK;
	Display.LS[4] = BLANK;
	Display.LS[5] = BLANK;
}

